package org.academiadecodigo.stringrays.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Server {


    private Socket serverSocket;
    private PrintWriter printWriter;
    private BufferedReader bReader;

    public BufferedReader getbReader() {
        return bReader;
    }

    public Server(int serverPort) throws IOException{

        serverSocket = new Socket("192.168.1.27", serverPort);
        printWriter = new PrintWriter(serverSocket.getOutputStream(), true);
        bReader = new BufferedReader(new InputStreamReader(serverSocket.getInputStream()));
    }

    public void sendMessage(String message){

        System.out.println("inServer sendMessage()");
        printWriter.println(message);
    }




    public synchronized String getOpponentMessage(){

        String line = "";
        try {
            System.out.println("Thread lock");
            line = bReader.readLine();
            System.out.println("post lock");
        }catch (IOException e){
            e.printStackTrace();
        }
        System.out.println("in opponentmessafe" + line );

        return line;
    }
}
