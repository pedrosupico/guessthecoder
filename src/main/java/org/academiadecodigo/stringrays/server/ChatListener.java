package org.academiadecodigo.stringrays.server;

import org.academiadecodigo.stringrays.client.chat.Chat;
import org.academiadecodigo.stringrays.client.game.Player;

import java.io.IOException;

public class ChatListener implements Runnable {

    private Server server;
    private Chat chat;
    private Player player;

    public ChatListener(Server server, Chat chat, Player player) {
        this.server = server;
        this.chat = chat;
        this.player = player;
    }


    @Override
    public void run() {

        String message;

        try {
            while ((message = server.getbReader().readLine()) != null) {

                String[] tokens = message.split("/");
                System.out.println(tokens[0]);

                if (message.length() < 1) {
                    continue;
                }
                if (tokens[0].equals("Opponent:  OppCardId")) {
                    System.out.println(Integer.parseInt(tokens[1]));
                    player.setOpponentCard(Integer.parseInt(tokens[1]));
                    continue;
                }
                if (tokens[0].equals("Opponent:  Winner")) {
                    player.winner();
                    continue;
                }
                if (tokens[0].equals("Opponent:  Looser")) {
                    player.looser();
                    continue;
                }
                chat.getTextlog2().setText(message);

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
