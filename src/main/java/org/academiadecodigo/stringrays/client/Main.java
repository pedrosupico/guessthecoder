package org.academiadecodigo.stringrays.client;

import org.academiadecodigo.stringrays.client.menus.StartMenu;

public class Main {

    public static void main(String[] args) {

        StartMenu startMenu = new StartMenu();
        startMenu.runStartMenu();
    }
}
