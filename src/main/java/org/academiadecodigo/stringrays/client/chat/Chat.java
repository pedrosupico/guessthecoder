package org.academiadecodigo.stringrays.client.chat;

import org.academiadecodigo.simplegraphics.graphics.Text;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.stringrays.client.game.Game;
import org.academiadecodigo.stringrays.client.game.Player;
import org.academiadecodigo.stringrays.server.ChatListener;
import org.academiadecodigo.stringrays.server.Server;

import java.util.ArrayList;

public class Chat extends Game implements KeyboardHandler, Runnable {

    private Keyboard keyboard;
    private Text text;
    private Text textLog1;
    private Text textLog2;
    private ArrayList<String> chatWords = new ArrayList<>();

    private ChatListener chatListener;
    private Thread thread;

    public Chat(Server server, Player player) {
        super (server);
        chatListener = new ChatListener(server, this, player);
        thread = new Thread(chatListener);

    }

    @Override
    public void run() {

        System.out.println("hello1");
        chatInit();
        thread.start();
        System.out.println("hello2");
    }

    public void chatInit() {

        text = new Text(1269, 780, "Some text");
        textLog1 = new Text(1269, 705, "text mytext");
        textLog2 = new Text(1269, 640, "Text Oponent");
        text.draw();
        textLog1.draw();
        textLog2.draw();
        keyboardSetup();
    }

    private void keyboardSetup() {

        keyboard = new Keyboard(this);

        int[] keys = {
                KeyboardEvent.KEY_ENTER,
                KeyboardEvent.KEY_SPACE,
                KeyboardEvent.KEY_A,
                KeyboardEvent.KEY_B,
                KeyboardEvent.KEY_C,
                KeyboardEvent.KEY_D,
                KeyboardEvent.KEY_E,
                KeyboardEvent.KEY_F,
                KeyboardEvent.KEY_G,
                KeyboardEvent.KEY_H,
                KeyboardEvent.KEY_I,
                KeyboardEvent.KEY_J,
                KeyboardEvent.KEY_K,
                KeyboardEvent.KEY_L,
                KeyboardEvent.KEY_M,
                KeyboardEvent.KEY_N,
                KeyboardEvent.KEY_O,
                KeyboardEvent.KEY_P,
                KeyboardEvent.KEY_Q,
                KeyboardEvent.KEY_R,
                KeyboardEvent.KEY_S,
                KeyboardEvent.KEY_T,
                KeyboardEvent.KEY_U,
                KeyboardEvent.KEY_V,
                KeyboardEvent.KEY_W,
                KeyboardEvent.KEY_X,
                KeyboardEvent.KEY_Y,
                KeyboardEvent.KEY_Z,
                KeyboardEvent.KEY_UP,
                KeyboardEvent.KEY_1,
                KeyboardEvent.KEY_DOWN,
                KeyboardEvent.KEY_LEFT,
                KeyboardEvent.KEY_RIGHT,
                KeyboardEvent.KEY_BACKSPACE,
                KeyboardEvent.KEY_SHIFT,
                KeyboardEvent.KEY_ESCAPE
        };

        for (int key : keys) {
            setKeybindPressed(key);
            setKeybindReleased(key);
        }
    }

    private void write(String ch) {
        chatWords.add(ch);
    }

    private String getChatText() {

        String result = "";

        for (String s : chatWords) {
            result += s;
        }
        return result;
    }

    public Text getTextlog2(){
        return textLog2;
    }


    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }

    private void setKeybindPressed(int event) {
        KeyboardEvent newBind = new KeyboardEvent();
        newBind.setKey(event);
        newBind.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(newBind);
    }

    private void setKeybindReleased(int event) {
        KeyboardEvent newBind = new KeyboardEvent();
        newBind.setKey(event);
        newBind.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);
        keyboard.addEventListener(newBind);
    }



    @Override
    public void keyPressed(KeyboardEvent ev) {
        switch (ev.getKey()) {
            case KeyboardEvent.KEY_ENTER:
                server.sendMessage(getChatText());
                textLog1.setText("ME :" + getChatText());
                text.setText(" ");
                chatWords.clear();
                break;
            case KeyboardEvent.KEY_A:
                write("A");
                text.setText(getChatText());
                break;
            case KeyboardEvent.KEY_B:
                write("B");
                text.setText(getChatText());
                break;
            case KeyboardEvent.KEY_C:
                write("C");
                text.setText(getChatText());
                break;
            case KeyboardEvent.KEY_D:
                write("D");
                text.setText(getChatText());
                break;
            case KeyboardEvent.KEY_E:
                write("E");
                text.setText(getChatText());
                break;
            case KeyboardEvent.KEY_F:
                write("F");
                text.setText(getChatText());
                break;
            case KeyboardEvent.KEY_G:
                write("G");
                text.setText(getChatText());
                break;
            case KeyboardEvent.KEY_H:
                write("H");
                text.setText(getChatText());
                break;
            case KeyboardEvent.KEY_I:
                write("I");
                text.setText(getChatText());
                break;
            case KeyboardEvent.KEY_J:
                write("J");
                text.setText(getChatText());
                break;
            case KeyboardEvent.KEY_K:
                write("K");
                text.setText(getChatText());
                break;
            case KeyboardEvent.KEY_L:
                write("L");
                text.setText(getChatText());
                break;
            case KeyboardEvent.KEY_M:
                write("M");
                text.setText(getChatText());
                break;
            case KeyboardEvent.KEY_N:
                write("N");
                text.setText(getChatText());
                break;
            case KeyboardEvent.KEY_O:
                write("O");
                text.setText(getChatText());
                break;
            case KeyboardEvent.KEY_P:
                write("P");
                text.setText(getChatText());
                break;
            case KeyboardEvent.KEY_Q:
                write("Q");
                text.setText(getChatText());
                break;
            case KeyboardEvent.KEY_R:
                write("R");
                text.setText(getChatText());
                break;
            case KeyboardEvent.KEY_S:
                write("S");
                text.setText(getChatText());
                break;
            case KeyboardEvent.KEY_T:
                write("T");
                text.setText(getChatText());
                break;
            case KeyboardEvent.KEY_U:
                write("U");
                text.setText(getChatText());
                break;
            case KeyboardEvent.KEY_V:
                write("V");
                text.setText(getChatText());
                break;
            case KeyboardEvent.KEY_W:
                write("W");
                text.setText(getChatText());
                break;
            case KeyboardEvent.KEY_X:
                write("X");
                text.setText(getChatText());
                break;
            case KeyboardEvent.KEY_Y:
                write("Y");
                text.setText(getChatText());
                break;
            case KeyboardEvent.KEY_Z:
                write("Z");
                text.setText(getChatText());
                break;
            case KeyboardEvent.KEY_SPACE:
                write(" ");
                text.setText(getChatText());
                break;
            case KeyboardEvent.KEY_1:
                write("?");
                text.setText(getChatText());
                break;
            case KeyboardEvent.KEY_BACKSPACE:
                if (chatWords.isEmpty()) {
                    break;
                }
                chatWords.remove(chatWords.size() - 1);
                text.setText(getChatText());
                break;
        }
    }




}
