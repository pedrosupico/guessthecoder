package org.academiadecodigo.stringrays.client.menus;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class LooserMenu implements KeyboardHandler {
    private Keyboard keyboard = new Keyboard(this);
    private Picture background;

    public void start() {

        keyboardKeys();
        background = new Picture(40, 40, "menus/youlose.jpg");
        background.draw();
    }


    public void addKeyboardEvent(int key, KeyboardEventType type) {
        KeyboardEvent event = new KeyboardEvent();
        event.setKey(key);
        event.setKeyboardEventType(type);
        keyboard.addEventListener(event);
    }

    public void keyboardKeys() {
        addKeyboardEvent(KeyboardEvent.KEY_ESCAPE, KeyboardEventType.KEY_PRESSED);
        addKeyboardEvent(KeyboardEvent.KEY_ESCAPE, KeyboardEventType.KEY_RELEASED);
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardevent) {
        switch (keyboardevent.getKey()) {
            case KeyboardEvent.KEY_ESCAPE:
                System.exit(0);
                break;
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }
}
