package org.academiadecodigo.stringrays.client.menus;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.stringrays.client.game.Game;
import org.academiadecodigo.stringrays.server.Server;

import java.io.IOException;

import static org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType.*;

public class StartMenu implements KeyboardHandler {


    private boolean iKeyPressed;
    private boolean escapeKeyPressed;
    private boolean enterKeyPressed;

    private Keyboard keyboard = new Keyboard(this);
    private InstructionsMenu instructionsMenu;
    private Game game;
    private Server server;

    public void runStartMenu() {
        instructionsMenu = new InstructionsMenu(this);
        try {
            server = new Server(11001);
        } catch (IOException e) {
            e.printStackTrace();
        }
        game = new Game(server);

        keyboardKeys();
        initStartMenu();
    }

    public void initStartMenu() {
        Picture startMenu = new Picture(40, 40, "menus/mainmenubg.jpg");
        startMenu.draw();

        while (true) {
            try {
                Thread.sleep(80);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (iKeyPressed) {
                instructionsMenu.startInstructionsMenu();
                break;
            }
            if (enterKeyPressed) {
                game.init();
                break;
            }
            if (escapeKeyPressed) {
                System.exit(1);
            }
        }
    }


    public void addKeyboardEvent(int key, KeyboardEventType type) {
        KeyboardEvent event = new KeyboardEvent();
        event.setKey(key);
        event.setKeyboardEventType(type);
        keyboard.addEventListener(event);
    }

    public void keyboardKeys() {
        addKeyboardEvent(KeyboardEvent.KEY_I, KEY_PRESSED);
        addKeyboardEvent(KeyboardEvent.KEY_I, KEY_RELEASED);
        addKeyboardEvent(KeyboardEvent.KEY_ENTER, KEY_PRESSED);
        addKeyboardEvent(KeyboardEvent.KEY_ENTER, KEY_RELEASED);
        addKeyboardEvent(KeyboardEvent.KEY_ESCAPE, KEY_PRESSED);
        addKeyboardEvent(KeyboardEvent.KEY_ESCAPE, KEY_RELEASED);

    }


    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_I:
                iKeyPressed = true;
                break;
            case KeyboardEvent.KEY_ESCAPE:
                escapeKeyPressed = true;
                break;

            case KeyboardEvent.KEY_ENTER:
                enterKeyPressed = true;
                break;
        }

    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_I:
                iKeyPressed = false;
                break;
            case KeyboardEvent.KEY_Q:
                escapeKeyPressed = false;
                break;
            case KeyboardEvent.KEY_S:
                enterKeyPressed = false;
                break;
        }

    }
}
