package org.academiadecodigo.stringrays.client.menus;


import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.simplegraphics.pictures.Picture;


public class InstructionsMenu implements KeyboardHandler {


    private Keyboard keyboard = new Keyboard(this);
    private StartMenu startMenu;
    private Picture background;

    //back to initial menu
    private boolean escPressed;


    public InstructionsMenu(StartMenu startMenu) {
        this.startMenu = startMenu;
    }

    //show instructions menu
    public void startInstructionsMenu() {
        keyboardKeys();

        background = new Picture(40, 40, "menus/instructionsmenubg.jpg");
        background.draw();

        returnInitialMenu();


    }

    public void returnInitialMenu() {
        try {
            while (!escPressed) {
                Thread.sleep(30);
            }
            if (escPressed) {
                background.delete();
                startMenu.initStartMenu();
            }
            //if(player2Conecct) {
            //game.start();
            //}
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public void addKeyboardEvent(int key, KeyboardEventType type) {
        KeyboardEvent event = new KeyboardEvent();
        event.setKey(key);
        event.setKeyboardEventType(type);
        keyboard.addEventListener(event);
    }

    public void keyboardKeys() {
        addKeyboardEvent(KeyboardEvent.KEY_S, KeyboardEventType.KEY_PRESSED);
        addKeyboardEvent(KeyboardEvent.KEY_S, KeyboardEventType.KEY_RELEASED);
        addKeyboardEvent(KeyboardEvent.KEY_Q, KeyboardEventType.KEY_PRESSED);
        addKeyboardEvent(KeyboardEvent.KEY_Q, KeyboardEventType.KEY_RELEASED);
        addKeyboardEvent(KeyboardEvent.KEY_M, KeyboardEventType.KEY_PRESSED);
        addKeyboardEvent(KeyboardEvent.KEY_M, KeyboardEventType.KEY_RELEASED);
        addKeyboardEvent(KeyboardEvent.KEY_ESCAPE, KeyboardEventType.KEY_PRESSED);
        addKeyboardEvent(KeyboardEvent.KEY_ESCAPE, KeyboardEventType.KEY_RELEASED);
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_ESCAPE:
                escPressed = true;
                break;

        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_ESCAPE:
                escPressed = false;
                break;

        }

    }
}



