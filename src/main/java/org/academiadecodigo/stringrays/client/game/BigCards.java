package org.academiadecodigo.stringrays.client.game;

public enum BigCards {

    PAULO(1, "cardsbig/paulo.jpg"),
    PEDRO(2, "cardsbig/pedro.jpg"),
    ZACARIAS(3, "cardsbig/zacarias.jpg"),
    JORGE(4, "cardsbig/tekman.jpg"),
    CARLOS(5, "cardsbig/carlos.jpg"),
    EDUARDO(6, "cardsbig/eduardo.jpg"),
    RICARDOSANTOS(7, "cardsbig/ricardo.jpg"),
    FILIPE(8, "cardsbig/flipflops.jpg"),
    DAVID(9, "cardsbig/david.jpg"),
    JAKHONGIR(10, "cardsbig/johny.jpg"),
    RUI(11, "cardsbig/rui.jpg"),
    JOAO(12, "cardsbig/madeira.jpg"),
    SID(13, "cardsbig/sid.jpg"),
    JOSUE(14, "cardsbig/zuzue.jpg"),
    ALEXANDRA(15, "cardsbig/alexandra.jpg"),
    LAIS(16, "cardsbig/lays.jpg"),
    DENISE(17, "cardsbig/denise.jpg"),
    MARIANA(18, "cardsbig/mariana.jpg"),
    RICARDODIAS(19, "cardsbig/ricky.jpg"),
    LUIS(20, "cardsbig/luis.jpg"),
    RUBEN(21, "cardsbig/ruben.jpg"),
    BERNARDO(22, "cardsbig/bernardo.jpg"),
    VANDO(23, "cardsbig/vando.jpg"),
    BENNY(24, "cardsbig/benny.jpg");

    private int id;
    private String frontImage;


    BigCards(int id, String frontImage) {

        this.id = id;
        this.frontImage = frontImage;

    }

    public int getId() {

        return id;
    }

    public String getCardFront() {

        return frontImage;
    }
}

