package org.academiadecodigo.stringrays.client.game;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.stringrays.client.menus.LooserMenu;
import org.academiadecodigo.stringrays.client.menus.WinnerMenu;
import org.academiadecodigo.stringrays.server.Server;

import java.util.ArrayList;

public class Player {

    private int cardId;
    private int opponentCard;
    private Picture playerCard;
    private Board board;
    private ArrayList<Card> personList;
    private ArrayList<BigCards> bigPersonList;
    private Server server;
    private WinnerMenu winnerMenu;
    private LooserMenu looserMenu;


    public Player(Server server) {
        this.server = server;
        randomCard();
    }

    public void init() {

        winnerMenu = new WinnerMenu();
        looserMenu = new LooserMenu();
        playerCard = new Picture(1210, 199, bigPersonList.get(cardId).getCardFront());
        playerCard.draw();
    }

    private void randomCard() {
        cardId = (int) (Math.random() * 24 + 1);
        System.out.println(cardId);
    }

    public void checkWinner(int id) {
        if (id == opponentCard) {
            server.sendMessage("Looser/");
            winner();
            return;
        }
        server.sendMessage("Winner/");
        looser();
    }

    public void setBoard(Board board) {
        this.board = board;
        personList = board.getPersonList();
        bigPersonList = board.getBigPersonList();
        server.sendMessage("OppCardId/" + cardId);
        System.out.println(opponentCard);
    }

    public void winner() {
        winnerMenu.start();
    }

    public void looser() {
        looserMenu.start();
    }

    public void setOpponentCard(int opponentCard) {
        this.opponentCard = opponentCard;
    }

    public int getCardId() {
        return cardId;
    }

    public int getOpponentCard() {
        return opponentCard;
    }
}
