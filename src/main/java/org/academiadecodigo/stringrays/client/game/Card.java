package org.academiadecodigo.stringrays.client.game;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Card {

    private int x;
    private int y;

    private Picture frontPicture;
    private Picture backPicture;
    private int id;

    String frontImagePath;
    private boolean turned;

    public Card(int x, int y, int id, String frontImagePath) {

        this.x = x;
        this.y = y;
        this.id = id;
        this.frontImagePath = frontImagePath;

        backPicture = new Picture(x, y, "cardssmall/cardBack.jpg");
        frontPicture = new Picture(x, y, frontImagePath);
        frontPicture.draw();
        turned = false;
    }

    public void turnCard() {
        if(!turned) {
            backPicture.draw();
            frontPicture.delete();
            turned = true;
            return;
        }
        frontPicture.draw();
        backPicture.delete();
        turned = false;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }


    public int getId() {
        return id;
    }
}
