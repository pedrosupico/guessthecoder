package org.academiadecodigo.stringrays.client.game;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.stringrays.client.chat.Chat;
import org.academiadecodigo.stringrays.server.Server;

public class Game implements KeyboardHandler {

    private Picture gameScreenBackground;
    private Board board;
    private Chat chat;
    protected Server server;
    private Player player;


    public Game(Server server) {
        keyboard();
        this.server = server;

    }

    public void init() {

        player = new Player(server);
        chat = new Chat(server, player);
        gameScreenBackground = new Picture(10, 10, "menus/whoswhogamelayout.jpg");
        gameScreenBackground.draw();

        board = new Board();
        board.init();

        player.setBoard(board);
        player.init();

        board.setPlayer(player);

        Thread threadChat = new Thread(chat);
        threadChat.start();
    }

    public void keyboard() {
        Keyboard k = new Keyboard(this);

        KeyboardEvent pressRight = new KeyboardEvent();
        pressRight.setKey(KeyboardEvent.KEY_RIGHT);
        pressRight.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        k.addEventListener(pressRight);

        KeyboardEvent pressDown = new KeyboardEvent();
        pressDown.setKey(KeyboardEvent.KEY_DOWN);
        pressDown.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        k.addEventListener(pressDown);

        KeyboardEvent pressLeft = new KeyboardEvent();
        pressLeft.setKey(KeyboardEvent.KEY_LEFT);
        pressLeft.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        k.addEventListener(pressLeft);

        KeyboardEvent pressUp = new KeyboardEvent();
        pressUp.setKey(KeyboardEvent.KEY_UP);
        pressUp.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        k.addEventListener(pressUp);

        KeyboardEvent pressPeriod = new KeyboardEvent();
        pressPeriod.setKey(KeyboardEvent.KEY_PERIOD);
        pressPeriod.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        k.addEventListener(pressPeriod);

        KeyboardEvent pressShift = new KeyboardEvent();
        pressShift.setKey(KeyboardEvent.KEY_SHIFT);
        pressShift.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        k.addEventListener(pressShift);
    }

    @Override
    public void keyPressed(KeyboardEvent e) {
        switch (e.getKey()) {
            case KeyboardEvent.KEY_RIGHT:
                board.moveCursor(0);
                break;
            case KeyboardEvent.KEY_DOWN:
                board.moveCursor(1);
                break;
            case KeyboardEvent.KEY_LEFT:
                board.moveCursor(2);
                break;
            case KeyboardEvent.KEY_UP:
                board.moveCursor(3);
                break;
            case KeyboardEvent.KEY_PERIOD:
                board.turnCard();
                break;
            case KeyboardEvent.KEY_SHIFT:
                board.guess();
                break;
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }
}
