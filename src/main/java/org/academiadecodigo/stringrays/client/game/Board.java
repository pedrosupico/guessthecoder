package org.academiadecodigo.stringrays.client.game;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

import java.util.ArrayList;

public class Board {

    private ArrayList<Card> cardList = new ArrayList<>();
    private ArrayList<SmallCards> personList = new ArrayList<>();
    private ArrayList<BigCards> bigPersonList = new ArrayList<>();
    private Rectangle cursor;


    private int PADDING = 60;
    private int cellWidth = 160;
    private int cellHeight = 210;
    private int x;
    private int y;
    private Player player;

    public Board() {
    }

    //initializes cursor and board, and populates board it with cards
    public void init() {

        for (SmallCards card : SmallCards.values()) {
            personList.add(card);
        }

        for (BigCards card : BigCards.values()) {
            bigPersonList.add((card));
        }

        int cellSpacingRow = 0;
        int cellSpacingCol = 0;
        int id = 0;

        for (y = 0; y < 4; y++) {
            cellSpacingRow = 0;
            for (x = 0; x < 6; x++) {
                cardList.add(new Card(x * cellWidth + PADDING + cellSpacingRow, y * cellHeight + PADDING + cellSpacingCol, personList.get(id).getId(), personList.get(id).getCardFront()));
                id++;
                cellSpacingRow += 20;
            }
            cellSpacingCol += 20;
        }

        cursor = new Rectangle(PADDING, PADDING, 160, 210);
        cursor.setColor(Color.WHITE);
        cursor.draw();
    }

    public void moveCursor(int direction) {
        switch (direction) {
            case 0:
                if (cursor.getX() < 900) {
                    cursor.translate(180, 0);
                }
                break;
            case 1:
                if (cursor.getY() < 690) {
                    cursor.translate(0, 230);
                }
                break;
            case 2:
                if (cursor.getX() > 70) {
                    cursor.translate(-180, 0);
                }
                break;
            case 3:
                if (cursor.getY() > 70) {
                    cursor.translate(0, -230);
                }
                break;
        }
    }

    public void turnCard() {
        for (int i = 0; i < cardList.size(); i++) {
            if (cursor.getX() == cardList.get(i).getX() && cursor.getY() == cardList.get(i).getY()) {
                cardList.get(i).turnCard();
            }
        }
    }

    public int getX() {
        return x * cellWidth;
    }

    public int getY() {
        return y * cellHeight;
    }

    public void input() {
        //have to spawn 5x5 cells with pictures and info
        //has to receive player input and has to send to game the user input

    }

    public void guess() {
        for (int i = 0; i < cardList.size(); i++) {
            if (cursor.getX() == cardList.get(i).getX() && cursor.getY() == cardList.get(i).getY()) {
                int cursorId = cardList.get(i).getId();
                player.checkWinner(cursorId - 1);
            }
        }
    }

    public ArrayList<Card> getPersonList() {
        ArrayList<Card> newList = new ArrayList<>();
        for (Card card : cardList) {
            newList.add(card);
        }
        return newList;
    }

    public ArrayList<BigCards> getBigPersonList() {
        ArrayList<BigCards> newList = new ArrayList<>();
        for (BigCards bigCard : bigPersonList) {
            newList.add(bigCard);
        }
         return newList;
    }


    public void setPlayer(Player player) {
        this.player = player;
    }
}