package org.academiadecodigo.stringrays.client.game;

public enum SmallCards {

    PAULO(1, "cardssmall/paulo_s.jpg", "cardssmall/cardBack.jpg"),
    PEDRO(2, "cardssmall/pedro_s.jpg", "cardssmall/cardBack.jpg"),
    ZACARIAS(3, "cardssmall/zacarias_s.jpg", "cardssmall/cardBack.jpg"),
    JORGE(4, "cardssmall/jorge_s.jpg", "cardssmall/cardBack.jpg"),
    CARLOS(5, "cardssmall/carlos_s.jpg", "cardssmall/cardBack.jpg"),
    EDUARDO(6, "cardssmall/eduardo_s.jpg", "cardssmall/cardBack.jpg"),
    RICARDOSANTOS(7, "cardssmall/ricardo_s.jpg", "cardssmall/cardBack.jpg"),
    FILIPE(8, "cardssmall/filipe_s.jpg", "cardssmall/cardBack.jpg"),
    DAVID(9, "cardssmall/david_s.jpg", "cardssmall/cardBack.jpg"),
    JAKHONGIR(10, "cardssmall/johny_s.jpg", "cardssmall/cardBack.jpg"),
    RUI(11, "cardssmall/rui_s.jpg", "cardssmall/cardBack.jpg"),
    JOAO(12, "cardssmall/madeira_s.jpg", "cardssmall/cardBack.jpg"),
    SID(13, "cardssmall/sid_s.jpg", "cardssmall/cardBack.jpg"),
    JOSUE(14, "cardssmall/josue_s.jpg", "cardssmall/cardBack.jpg"),
    ALEXANDRA(15, "cardssmall/alexandra_s.jpg", "cardssmall/cardBack.jpg"),
    LAIS(16, "cardssmall/lais_s.jpg", "cardssmall/cardBack.jpg"),
    DENISE(17, "cardssmall/denise_s.jpg", "cardssmall/cardBack.jpg"),
    MARIANA(18, "cardssmall/mariana_s.jpg", "cardssmall/cardBack.jpg"),
    RICARDODIAS(19, "cardssmall/ricky_s.jpg", "cardssmall/cardBack.jpg"),
    LUIS(20, "cardssmall/luis_s.jpg", "cardssmall/cardBack.jpg"),
    RUBEN(21, "cardssmall/ruben_s.jpg", "cardssmall/cardBack.jpg"),
    BERNARDO(22, "cardssmall/bernardo_s.jpg", "resourcessmall/cards/cardBack.jpg"),
    VANDO(23, "cardssmall/vando_s.jpg", "cardssmall/cardBack.jpg"),
    BENNY(24, "cardssmall/benny_s.jpg", "cardssmall/cardBack.jpg");

    private int id;
    private String frontImage;
    private String backImage;

    SmallCards(int id, String frontImage, String backImage) {

        this.id = id;
        this.frontImage = frontImage;
        this.backImage = backImage;
    }

    public int getId() {

        return id;
    }

    public String getCardFront() {

        return frontImage;
    }
}
